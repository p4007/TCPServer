package mx.net;

import mx.proceso.Proceso;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;

public class Server implements Serializable {

    public static void main(String[] args) {
        try {
            Server server = new Server();
            server.start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void start() throws IOException {
        ServerSocket server = null;
        Socket sc = null;
        ObjectInputStream in = null;
        ObjectOutputStream out = null;
        LinkedList<String> stringList = new LinkedList();

        try {
            server = new ServerSocket(7080);
            System.out.println("mx.net.Server start");

            while (true) {
                sc = server.accept();
                System.out.println("Recibiendo peticion: " + sc.getInetAddress());

                in = new ObjectInputStream(sc.getInputStream());
                out = new ObjectOutputStream(sc.getOutputStream());

                String action = (String) in.readObject();
                String[] mesg = action.split("\\|");
                System.out.println("Ejecutando accion: " + mesg[0]);

                Proceso hilo = new Proceso(action, stringList);
                hilo.start();
                hilo.join(1000);
                out.writeObject(stringList);
            }
        } catch (IOException | ClassNotFoundException | InterruptedException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                in.close();
            }

            if (out != null) {
                out.close();
            }

            if (sc != null) {
                sc.close();
                System.out.println("Conexion cerrada");
            }
        }
    }
}
