package mx.net;

import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.List;

public class Cliente implements Serializable {

    @Getter
    @Setter
    private int opc;
    @Getter
    @Setter
    private String mensaje;

    public void start() throws IOException {

        System.out.println("Client start");

        ObjectInputStream in = null;
        ObjectOutputStream out = null;

        Socket sc = null;

        try {
            sc = new Socket("127.0.0.1", 7080);
            out = new ObjectOutputStream(sc.getOutputStream());
            in = new ObjectInputStream(sc.getInputStream());

            out.writeObject((opc == 1 ? "add" : "remove") + "|" + mensaje);

            List<String> res = (List<String>) in.readObject();
            System.out.println("Actualizando contenido...");
            if (res != null) {
                res.forEach(System.out::println);
            }
        } catch (IOException | ClassNotFoundException io) {
            System.out.println(io.getMessage());
        } finally {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }

            if (sc != null) {
                sc.close();
            }
        }
    }
}
