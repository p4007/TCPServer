package mx.proceso;

import java.util.LinkedList;

public class Proceso extends Thread {


    private final LinkedList<String> peticiones;
    private final String msg;

    public Proceso(String msg, LinkedList<String> peticiones) {
        this.msg = msg;
        this.peticiones = peticiones;
    }


    @Override
    public void run() {

        synchronized (peticiones) {
            String[] mesg = this.msg.split("\\|");
            if (mesg[0].equals("add")) {
                this.addDato(mesg[1]);
                System.out.println("Se agrego contenido " + "[" + mesg[1] + "]" + "..." + this.getName());
                this.printData();
            } else if (mesg[0].equals("remove")) {
                this.removeDato(mesg[1]);
                System.out.println("Se elimino contenido " + "[" + mesg[1] + "]" + "..." + this.getName());
                this.printData();
            }
        }
    }

    public synchronized void addDato(String dato) {
        peticiones.add(dato);
        peticiones.notify();
    }

    public synchronized void removeDato(String dato) {
        if (peticiones.size() > 0) {
            peticiones.remove(dato);
            peticiones.notify();
        }

    }

    public void printData(){
        System.out.println("**Contenido**");
        peticiones.forEach(System.out::println);
    }
}
