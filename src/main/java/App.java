import mx.net.Cliente;

import java.io.IOException;
import java.util.Scanner;

public class App {

    public static void main(String[] args) throws IOException {

        int opc = 0;
        Scanner scanner = new Scanner(System.in);
        Cliente cliente = new Cliente();

        do {
            System.out.println("***Menu***");
            System.out.println("1. Agregar elemento al arreglo (add).");
            System.out.println("2. Eliminar un elemento del arreglo (remove).");
            System.out.println("3. Salir");

            opc = scanner.nextInt();
            if(opc != 3) {
                System.out.println("");
                System.out.print("Ingresa un dato: ");
                String dato = scanner.next();

                if (dato != null) {
                    cliente.setOpc(opc);
                    cliente.setMensaje(dato);
                    cliente.start();
                }
            }
        } while (opc != 3);
    }
}
